####
#### Temporary layer to prepare installation
####
FROM ubuntu:bionic AS build

MAINTAINER EOLE <eole@ac-dijon.fr>

USER root

# Packages required to build
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update -y && apt-get install -y curl

ARG CONTAINERPILOT_VERSION=3.4.3
ARG CONTAINERPILOT_CHECKSUM=e8258ed166bcb3de3e06638936dcc2cae32c7c58

RUN curl -Lso /tmp/containerpilot.tar.gz \
         "https://github.com/joyent/containerpilot/releases/download/${CONTAINERPILOT_VERSION}/containerpilot-${CONTAINERPILOT_VERSION}.tar.gz" \
    && echo "${CONTAINERPILOT_CHECKSUM}  /tmp/containerpilot.tar.gz" | sha1sum -c \
    && tar zxf /tmp/containerpilot.tar.gz -C /tmp


####
#### Target layer
####
FROM crossbario/crossbar:pypy3-18.11.2

MAINTAINER EOLE <eole@ac-dijon.fr>

USER root

# Install tools from build layer
COPY --from=build /tmp/containerpilot /usr/local/bin
RUN chmod +x /usr/local/bin/*

# Manage container with ContainerPilot
CMD ["/usr/local/bin/containerpilot", "-config", "/etc/containerpilot.json5"]
COPY containerpilot.json5 /etc/containerpilot.json5

ENTRYPOINT [""]
